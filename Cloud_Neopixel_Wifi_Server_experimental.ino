// Philip's rainbow cloud
//
//  Code for WEMOS D1 mini or similar (https://www.wemos.cc)
//  checks EEPROM for ssid and pw
//  tries to connect to Wifi as Client with ssid and pw from EEPROM
//  if not able to: will open as an Access point with SSID "Cloud" and PW "yeahyeahyeah"
//  after connecting or opening a WIFI will open a http server that you can connect to in order to control the lights connected to D4

/*
LED Effects
released under Creative Commons Attribution 4.0
by bitluni 2016
https://creativecommons.org/licenses/by/4.0/
Attribution means you can use it however you like as long you
mention that it's base on his stuff.
I'll be pleased if you'd do it by sharing http://youtube.com/bitlunislab
*/

/*
Server and Access Point as well as EEPROM ssid and pw chooser by
Philip Steimel 2017
released under Creative Commons Non-Commercial Attribution 4.0
*/

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>

#include <Adafruit_NeoPixel.h>

#include <EEPROM.h>

#include "PinStates.h"
#include "LedStates.h"
#include "Fader.h"
#include "RainbowFunction.h"
#include "SimpleRGBFunction.h"
#include "WaveFunction.h"
#include "RF.h"


///////////////////////////////////////////////////////////////////
// helper vars for EEPROM READ/WRITE >>>>

// start reading from the first byte (address 0) of the EEPROM
int addressVar1 = 0;
int addressVar2 = 50;
int sizeVar1 = 25;
int sizeVar2 = 25;
int sizeVar = 512;
int addrW = 0;
char variable1[25] = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}; // container for read
char variable2[25] = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}; // container for read

String inputStringPW = "DAS IST kein TEST!"; // this one simulates incoming String from javascript
String inputStringSSID = "Hexaflexagon"; // this one simulates incoming String from javascript

char someString[] = {'B','L','U','M','E','N','!','\0'};
// this should be filled from inputString including a \0 character at the end
// then it should be turned into a

// <<<< END helper vars for EEPRO READ/WRITE
///////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////
// BEGIN vars WIFI >>>>>>>>>>>>>>>>>
char ssid[] = "DianasCloud";
char password[] = "happybirthday!";

//char ssidClient[] = "Hexaflexagon";
//char passwordClient[] = "Arduino2010";
char* ssidClient = "test";
char* passwordClient = "testibumspasswort";

String newssid = "";
String newpw = "";

ESP8266WebServer server(80);

const int LED_PIN = D4;
const int LED_COUNT = 7;

const int RF_OSC = 200;

const String configpage = "<!DOCTYPE html><html><head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"></head><style>.textinput{font-size:1.5em;background-color:rgb(200,220,220);margin-top:20px;height:1.5em}.button{background-color:#4CAF50;border:none;color:white;padding:15px 32px;text-align:center;text-decoration:none;display:inline-block;font-size:1em;margin:4px 2px;cursor:pointer}</style><body style='font-family: sans-serif; font-size: 2.5em'><div align=\"center\" width=\"100%\"><p style=\"font-size:0.6em\">Do you want to connect your cloud with your own wifi network?<br> No problemo!<br><br>Just submit your wifi's name (ssid) and password and press submit:</p><form id=\"frm1\"> ssid:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input class=\"textinput\" type=\"text\" name=\"fname\" value=\"\"><br> password: <input class=\"textinput\" type=\"text\" name=\"lname\" value=\"\"><br><br></form><button class=\"button\" onclick=\"myFunction()\">submit</button></div> <script>function myFunction(){var x=document.getElementById(\"frm1\");var ssid=x[0].value;var pw=x[1].value;console.log(\"ssid: \"+ssid+\"npassword: \"+pw);window.location=\"./config?ssid=\"+ssid+\"&pw=\"+pw;}</script> </body></html>";

const String redirect1 = "<!DOCTYPE html><html><head><!-- HTML meta refresh URL redirection --><meta http-equiv=\"refresh\" content=\"0; url=http://";
const String redirect2 = "\"></head><body></body></html>";
String redirect = ""; // getting filled with IP
//  <<<<<<<<<<< END vars WIFI
//////////////////////////////////////////////


Adafruit_NeoPixel strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);

LedStates currentLedStates(strip);
LedStates targetLedStates(strip);
Fader<LedStates> ledFader(currentLedStates, targetLedStates);
PinStates currentPinStates;
PinStates targetPinStates;
Fader<PinStates> pinFader(currentPinStates, targetPinStates);


void handleRoot()
{
  String message = "<html><head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"></head>";
  message += "<style>body {  font-family: Futura, Tahoma;  font-size: 18px;  background-color: rgba(50,50,50,1);  /*background-repeat: no-repeat;  background-position: center;  background-size: 100vh;*/}p,h1,h3 {  margin: 0;  padding: 0;  top: 0;  bottom: 0;}h3 {  line-height: 17px;  color: rgba(240,240,255,0.9);}h1 {    background-color: rgba(0,0,0,0.2);    animation-name: fade2orange;    animation-duration: 13.5s;    animation-direction: alternate;    animation-iteration-count: infinite;}@keyframes fade2orange{    0%   {color: rgba(255, 0, 255, 0.6);}    50%  {color: rgba(255, 255, 255, 0.8);}    100% {color: rgba(30,255,255,0.7);}}button {  margin: 0; outline:none}.button:hover {  animation-name: breathe;  animation-duration: 4s;  animation-iteration-count: infinite;}@keyframes breathe {  0% {    -webkit-transform: scale(1);    -ms-transform: scale(1);    transform: scale(1);  }  33% {    -webkit-transform: scale(0.9);    -ms-transform: scale(0.9);    transform: scale(0.9);  }  100% {    -webkit-transform: scale(1);    -ms-transform: scale(1);    transform: scale(1);  }}@keyframes breatheDeep {  0% {    -webkit-transform: scale(1);    -ms-transform: scale(1);    transform: scale(1);  }  50% {    -webkit-transform: scale(1.4);    -ms-transform: scale(1.4);    transform: scale(1.4);  }  100% {    -webkit-transform: scale(1);    -ms-transform: scale(1);    transform: scale(1);  }}.button {  background-color: rgba(0,255,0,0.5);border:none;color:white;padding:15px 32px;  text-align: center;text-decoration:none;display: inline-block;font-size: 16px;  margin: 4px 2px;cursor: pointer;  border-radius: 25vw;}.button2 {background-color: rgba(0,0,255,0.5)}.button3 {background-color: rgba(255,0,0,0.5)}.button4 {background-color: #e7e7e7; color: black;}.button5 {background-color: #555555;}.button6 {background-color: rgba(0,0,50,0.5); font-size: 0.7em}.footer {font-size: 0.7em; color: white; background-color: rgba(0,0,0,0.2);}.footer a {color: lightgrey}.footer:hover {animation-name: breatheDeep; animation-duration: 4s; animation-direction: forward}</style>";
  message += "<body style='font-family: sans-serif; font-size: 18px'><div align=\"center\"><h3>Dianas<br>Birthday</h3><h2>Cloud Service</h2>";
  message += "<a href=\"/configpage\" align=\"right\"><p style=\"font-size:0.3em\">config</a><br>";
  message += "<button class=\"button\" onclick=\"ajax('/rainbow?fade=3000')\">Rainbow</button><br><br>";
  message += "<button class=\"button button2\" onclick=\"ajax('/wave?r=255&g=255&b=255&fade=15000')\">Wave</button><br><br>";
  message += "<button class=\"button button4\" onclick=\"setLEDs(255,255,255,255)\">White</button><br><br>";
  message += "<button class=\"button button3\" onclick=\"setLEDs(15,0,0,0)\">+</button><button class=\"button\" onclick=\"setLEDs(0,15,0,0)\">+</button><button class=\"button button2\" onclick=\"setLEDs(0,0,15,0)\">+</button><!--<button class=\"button button3\" onclick=\"setLEDs(0,0,0,15)\">+</button>--><br><br>";
  message += "<button class=\"button button3\" onclick=\"setLEDs(-15,0,0,0)\">-</button><button class=\"button\" onclick=\"setLEDs(0,-15,0,0)\">-</button><button class=\"button button2\" onclick=\"setLEDs(0,0,-15,0)\">-</button><!--<button class=\"button button3\" onclick=\"setLEDs(0,0,0,-15)\">-</button>--><br><br>";
  message += "<button class=\"button button3\" onclick=\"ajax('/setleds?r=0&b=0&g=0');setLEDs(-255,-255,-255,-255)\">OFF</button><br><br>";
  message += "<script>var r = 0;var g=0;var b=0; var w=0;function setLEDs(red,green,blue,white){r+=red;g+=green;b+=blue;w+=white;checkRGB();ajax('/setleds?r='+r+'&g='+g+'&b='+b+'&w='+w+'');}function ajax(input){var xhttp = new XMLHttpRequest();xhttp.open(\"GET\", input, true);xhttp.send();}function checkRGB(){if(r>255){r=255;}if(g>255){g=255;}if(b>255){b=255;}if(w>255){w=255;}if(r<0){r=0;}if(g<0){g=0;}if(b<0){b=0;};if(w<0){w=0;};console.log(r+\" \"+g+\" \"+b+\" \"+w+\" \");}</script>";

  server.send(200, "text/html", message);
}

void handleNotFound(){
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET)?"GET":"POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i=0; i<server.args(); i++){
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
}

int getArgValue(String name)
{
  for (uint8_t i = 0; i < server.args(); i++)
    if(server.argName(i) == name)
      return server.arg(i).toInt();
  return -1;
}

bool checkFadeAndSetLedFunction(LedFunction *f)
{
  int fade = getArgValue("fade");
  if(fade > -1)
  {
    targetLedStates.setFunction(f);
    ledFader.start(fade);
  }
  else
    currentLedStates.setFunction(f);
}

String DisplayAddress(IPAddress address)
{
 return String(address[0]) + "." +
        String(address[1]) + "." +
        String(address[2]) + "." +
        String(address[3]);
}


////////////////////////////////////////////
//  EEPROM FUNCTIONS >>>>>>>>>>

void writeEEPROM(char input[], int start, int writeSize)
{
  Serial.println("write starts:");

  for ( int i = 0; i < writeSize; ++i )
  {
   EEPROM.write ( i+start, input [ i ] );
   delay(100);
  }
  EEPROM.commit();

  Serial.println("write ended \n");
}

void writeEEPROMfromString(String input, int start, int writeSize)
{
  Serial.println("converting String to char array");

  char output[writeSize];
  input.toCharArray(output , input.length()+2);


  Serial.println("write starts:");

  for ( int i = 0; i < writeSize; ++i )
  {
   Serial.print(output[i]);
   EEPROM.write ( i+start, output [ i ] );
   delay(100);
  }
  EEPROM.commit();

  Serial.println("\nwrite ended \n");
  readEEPROM2(start, writeSize);

}


char* readEEPROM(char output[], int addr, int readSize)
{
  for (int i = 0; i < readSize; i++)
  {
    char var = EEPROM.read(addr+i);
    delay(50);
    Serial.print(int(var));Serial.print(" : ");Serial.println(var);
    if (var != 0)
    {
      output[i] = var;
    }
    else
    {
      output[i] = var;    // add End character to String and
      return output;      // return new string
    }

  }
}

void readEEPROM2(int addrStart, int addrEnd)
{
  int values[addrEnd-addrStart];

  for (int i = 0; i <= (addrEnd-addrStart); i++)
  {
    values[i] = EEPROM.read(addrStart+i);
    Serial.print(values[i]);Serial.print(" : ");Serial.println(char(values[i]));
    delay(50);
  }
  Serial.println("\n");
}


void printArray(char input[], int readSize)
{
  String output = String(input); // yes! cool! so we got our String to fill into ssid and pw!!! <3
  Serial.println(output);

  /*for (int i = 0; i < readSize; i++)
  {
    Serial.print(int(input[i])); Serial.print(" : ");
    Serial.println(input[i]);
  }*/
}



void initEEPROM()
{
  inputStringPW.toCharArray(someString , inputStringPW.length()+2);

  // initialize serial  and wait for port to open:
  while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }
  EEPROM.begin(sizeVar);

  //Serial.println("first read:");
  //readEEPROM2(5, 10);

  //Serial.println("first read end"); Serial.println(); delay(1000);

  //writeEEPROM(someString,5,sizeVar1);
  //writeEEPROMfromString(inputStringSSID,5,sizeVar1);

  delay (1000); Serial.println("reading ssid:");

  ssidClient = readEEPROM(variable1,addressVar1,sizeVar1);
//  Serial.println("printing 1nd read var: \n");
  printArray(variable1, sizeVar1);


  delay (1000); Serial.println("reading pw:");

  //readEEPROM2(5, 10);
  passwordClient = readEEPROM(variable2,addressVar2,sizeVar2);
//  Serial.println("printing 2nd read var: \n");
  printArray(variable2, sizeVar2);
}


////////////////////////////////////////////
// <<<<<<<<<<< END EEPROM FUNCTIONS


//////// LOGIN WIFI //////////

bool loginAsClient()
{
    Serial.print("trying to connect to: "); Serial.print(ssidClient); Serial.print(" with pw: ");Serial.println(passwordClient);
    WiFi.begin(ssidClient, passwordClient); // in case we want to be Client

    int count = 0;
    // Wait for connection // works only if client!
    while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      if (count < 30)
      {
        Serial.print(".");
        count++;
      }
      else
      {
        Serial.println("could not connect to WIFI. Opening my own...");
        return false;
      }

    }
    return true;
}

bool loginAsAP()
{
  WiFi.softAP(ssid, password); // in case we want to be Access Point
}

/////////////////////////////////////////////
////////////// MAIN /////////////////////////
/////////////////////////////////////////////

IPAddress myIP;

void setup(void)
{
  Serial.begin(115200);

  initEEPROM(); // get ssidClient and passwordClient from EEPROM

  if (loginAsClient())
  {
    myIP = WiFi.localIP(); // in case we want to be Client
  }
  else
  {
    loginAsAP();
    myIP = WiFi.softAPIP(); // in case we want to be Access Point
  }
  //WiFi.begin(ssid, password); // in case we want to be Client
  //WiFi.begin(ssid); // in case we want to be Client w/o password
  Serial.println("");

/*
  // Wait for connection // works only if client!
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  */
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(myIP);

  redirect = redirect1 + DisplayAddress(myIP) + redirect2;

  Serial.println(redirect);
  Serial.println();



  //find it as http://lights.local
  if (MDNS.begin("lights"))
  {
    Serial.println("MDNS responder started");
  }



  server.on("/", handleRoot);

  server.on("/configpage", [](){
      server.send(200, "text/html", configpage);
    });

  server.on("/config", [](){
      server.send(200, "text/html", redirect);
      if(server.hasArg("ssid"))
      {
       newssid = server.arg("ssid");
       Serial.print("\n\n####\nssid: ");
       Serial.println(newssid);
       Serial.print("\n####\n");
       // write the new ssid into EEPROM:
       writeEEPROMfromString(newssid,addressVar1,sizeVar1);
      }
      if(server.hasArg("pw"))
      {
       newpw = server.arg("pw");
       Serial.print("\n\n####\npassword: ");
       Serial.println(newpw);
       Serial.print("\n####\n");
       writeEEPROMfromString(newpw,addressVar2,sizeVar2);
      }

      //configWifi(); // THIS IS FOR actually connecting to a choosen Wifi
    });

  server.on("/rainbow", [](){
    //server.send(200, "text/plain", "rainbow");
    server.send(200, "text/html", redirect);
    checkFadeAndSetLedFunction(new RainbowFunction());
  });

  server.on("/wave", [](){
    server.send(200, "text/html", redirect);
    WaveFunction *f = new WaveFunction();
    f->init(server);
    checkFadeAndSetLedFunction(f);
  });

  server.on("/setleds", [](){
    server.send(200, "text/html", redirect);
    SimpleRGBFunction *f = new SimpleRGBFunction();
    f->init(server);
    checkFadeAndSetLedFunction(f);
  });

  server.on("/ledsoff", [](){
    server.send(200, "text/html", redirect);
    checkFadeAndSetLedFunction(new SimpleRGBFunction());
  });


  server.on("/pinsoff", [](){
    server.send(200, "text/html", redirect);
    currentPinStates.setAllTo(0);
    currentPinStates.commit();
  });

  server.onNotFound(handleNotFound);

  server.begin();
  Serial.println("HTTP server started");

  strip.begin();
  strip.show(); // Initialize all pixels to 'off'
}




void loop(void)
{
  server.handleClient();

  MDNS.update();

  currentLedStates.render();
  if(ledFader.active)
    targetLedStates.render();
  if(!ledFader.fade())
    currentLedStates.commit();
  pinFader.fade();
}
